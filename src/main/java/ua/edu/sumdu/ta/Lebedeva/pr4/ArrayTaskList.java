package ua.edu.sumdu.ta.Lebedeva.pr4;

public class ArrayTaskList extends AbstractTaskList {

    public int size;
    public int from;
    public int to;
    private final String title = "[EDUCT][TA]";

    /**
     * Constructor that  counts the number of task and and an array is defined for the abstract class
     */
    public ArrayTaskList() {
        int kolvo = 0;
        super.elements = new Task[10];
        kolvo++;
    }


    /**
     * method for increasing the capacity if the array is full
     *
     * @return new array with increased capacity
     */
    private Task[] increaseCapacity() {
        final int increase = 15;
        Task[] elements1 = new Task[elements.length + increase];
        if (size() >= 0) System.arraycopy(elements, 0, elements1, 0, elements.length);
        return elements1;
    }

    /**
     * Override method for add new non-unique task
     *
     * @param task
     */
    @Override
    public void add(Task task) {
        int size = size();
        if (size == elements.length) {
            elements = increaseCapacity();
        }
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == null) {
                elements[i] = task;
                elements[i].setTitle(title + elements[i].getTitle());
                break;
            }
        }
    }

    /**
     * Override method for remove all task equals to the input
     *
     * @param task
     */
    @Override
    public void remove(Task task) {
        int size;
        size = elements.length;
        for (int i = 0; i < size; i++) {
            if (task.equals(elements[i])) {
                size--;
                while ((elements[i] != null) || (elements[i + 1] != null)) {
                    elements[i] = elements[i + 1];
                    i++;
                }
            }
        }
    }

    /**
     * method which returns array of task that the notification time is between from and to
     *
     * @param from start of gap
     * @param to   end of the gap
     * @return new array containing elements that fall into the gap
     */
    public Task[] incoming(int from, int to) {
        this.from = from;
        this.to = to;
        this.size = size();
        int i = 0;
        while (i < size) {
            if ((elements[i].nextTimeAfter(from) == -1) || (elements[i].nextTimeAfter(from) > this.to)) {
                size--;
                if (size - i >= 0) System.arraycopy(elements, i + 1, elements, i, size - i);
            } else {
                i++;
            }
        }
        Task[] additional = new Task[size];
        for (int f = 0; f < size + 1; f++) {
            System.arraycopy(elements, 0, additional, 0, size);
        }
        return additional;
    }

    /**
     * Override method gets the task under the specified index
     *
     * @param index
     * @return elements
     */
    @Override
    public Task getTask(int index) {
        return elements[index];
    }
}
