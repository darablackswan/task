package ua.edu.sumdu.ta.Lebedeva.pr5;

public abstract class AbstractTaskList {

    public Task[] elements;

    /**
     * abstract method for add new non-unique task
     *
     * @param task
     */
    public abstract void add(Task task);

    /**
     * abstract method for remove all task equals to the input
     *
     * @param task
     */
    public abstract void remove(Task task);

    /**
     * method which returns the number of task
     *
     * @return size is number of task
     */
    public int size() {
        int size = 0;
        for (Task task : elements) {
            if (task != null) {
                size++;
            }
        }
        return size;
    }

    /**
     * abstract method gets the task under the specified index
     *
     * @param index
     * @return
     */
    public abstract Task getTask(int index);

    /**
     * abstract method returns list of task, which have notification time between from and to
     *
     * @param from start notification
     * @param to   end notification
     * @return list of task
     */
    public abstract Task[] incoming(int from, int to);
}
